#!/bin/bash
source scripts/env.sh
update-mydeb.sh
apt update
apt install $PROJECT_NAME
nginx -t
systemctl daemon-reload
systemctl restart nginx
systemctl restart $PROJECT_NAME
