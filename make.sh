#!/bin/bash
source scripts/env.sh
echo $((1 + $(cat env/SUB_VERSION))) > env/SUB_VERSION
LOCAL_REP_COUNT=$(dpkg -l | grep local-rep | wc -l)

if [[  $LOCAL_REP_COUNT = "0"  ]]
then echo "install package local-rep, exec: git clone git@gitlab.com:Defens1k/local-rep.git"; exit 1
else echo "local-rep pkg found"
fi

scripts/deb.sh
cd filesystem
dpkg-deb --build . ../$(echo $PROJECT_NAME)_$(echo $VERSION)_all.deb
cp ../*.deb ../build
mv ../*.deb /usr/local/mydebs
