#!/bin/bash
source scripts/env.sh
echo $PROJECT_NAME
cat > filesystem/etc/systemd/system/$PROJECT_NAME.service <<EOF
[Unit]
Description=$DESCRIPTION

[Service]
ExecStart=/opt/bot/$PROJECT_NAME-start.sh
ExecStop=/opt/bot/$PROJECT_NAME-stop.sh
Type=simple

[Install]
WantedBy=multi-user.target
EOF

