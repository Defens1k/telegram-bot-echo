#!/bin/bash
source scripts/env.sh

#DEBIAN block
scripts/changelog.sh
scripts/control.sh
#DEBIAN block



#systemd block
mkdir filesystem/etc
mkdir filesystem/etc/systemd
mkdir filesystem/etc/systemd/system
scripts/systemd.sh
scripts/systemd_execs.sh
#systemd block

scripts/nginx.sh
