#!/bin/bash
source scripts/env.sh

mkdir filesystem/etc/nginx/conf.d 
mkdir filesystem/etc/nginx/bots-enabled

cat > filesystem/etc/nginx/conf.d/telegram-bot.conf <<EOF
server {
  listen 443 ssl;
  listen [::]:443 ssl;
  server_name $FE_IP , $HOSTNAME;

  ssl_certificate /etc/nginx/ssl/certs/telegram-bot.pub.pem;
  ssl_certificate_key /etc/nginx/ssl/private/telegram-bot.priv.key;

  include /etc/nginx/bots-enabled/*;
}
EOF

cat > filesystem/etc/nginx/bots-enabled/$PROJECT_NAME.conf <<EOF
  location /api/v1/$PROJECT_NAME {
    proxy_pass http://$BE_IP:$BE_PORT/api/v1/$PROJECT_NAME;

    access_log /var/log/nginx/bot.access.log;
    error_log /var/log/nginx/bot.error.log;

  }
EOF
