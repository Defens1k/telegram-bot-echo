#!/bin/bash
source scripts/env.sh

cat > filesystem/opt/bot/$PROJECT_NAME-start.sh <<EOF
#!/bin/bash
#sudo mysql -u root 		-e "CREATE DATABASE IF NOT EXIST BOT;"
#sudo mysql -u root -d BOT 	-e "CREATE TABLE IF NOT EXIST TICKETS( \
#    ticket_id INT AUTO_INCREMENT NOT_NULL,
#    string NOT_NULL VARCHAR(255),
#    create_time DATETIME NOT_NULL,
#    dest_time DATETIME NOT_NULL,
#    group VARCHAR(255) NOT_NULL,
#    type VARCHAR(255) NOT_NULL,
#    PRIMARY_KEY(ticket_id),
#    );
#"

python3 /opt/bot/$PROJECT_NAME.py $BE_PORT
EOF

cat > filesystem/opt/bot/$PROJECT_NAME-stop.sh <<EOF
#!/bin/bash
for i in \$(ps -leax | grep "python3 /opt/bot/$PROJECT_NAME.py" | awk '{print \$3}')
do
  kill -15 \$i
done
exit 0
EOF

chmod +x filesystem/opt/bot/$PROJECT_NAME-start.sh
chmod +x filesystem/opt/bot/$PROJECT_NAME-stop.sh
