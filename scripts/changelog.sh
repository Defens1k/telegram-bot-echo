#!/bin/bash
source scripts/env.sh
cat > filesystem/DEBIAN/changelog <<EOF
$PROJECT_NAME ($VERSION) unstable; urgency=medium

  * Some change

 -- author <$GITLAB_USER_EMAIL>  $(date '+%a, %d %h %Y %H:%M:%S %z')
EOF

