#!/bin/bash
source scripts/env.sh

cat > filesystem/DEBIAN/control <<EOF
Package: $PROJECT_NAME
Version: $VERSION
Maintainer: author <s.russkikh@corp.mail.ru>
Architecture: all
Section: misc
Description: $DESCRIPTION
Depends: $DEPENDS
Priority: optional
EOF

