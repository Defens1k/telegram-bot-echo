import asyncio
import aiohttp
from aiohttp import web
import json
import sys

#
#
#
#

TOKEN = '902256952:AAG3UnD8EA_NFwOQQKBtLgNsqYGARRCdeUU'
API_URL = 'https://api.telegram.org/bot%s/sendMessage' % TOKEN
print(API_URL)
async def handler(request):
    data = await request.json()
    print("200")
    headers = {
        'Content-Type': 'application/json'
    }
    message = {
        'chat_id': data['message']['chat']['id'],
        'text': data['message']['text']
    }
    async with aiohttp.ClientSession(loop=loop) as session:
        async with session.post(API_URL,
                                data=json.dumps(message),
                                headers=headers) as resp:
            try:
                assert resp.status == 200
            except:
                return web.Response(status=500)
    return web.Response(status=200)

async def init_app(loop):
    app = web.Application(loop=loop, middlewares=[])
    app.router.add_post('/api/v1/telegram-bot-echo', handler)
    return app

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        app = loop.run_until_complete(init_app(loop))
        web.run_app(app, host='0.0.0.0', port=sys.argv[1])
    except Exception as e:
        print('Error create server: %r' % e)
    finally:
        pass
    loop.close()
